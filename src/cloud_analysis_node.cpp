#include "cloud_analysis/MyCloud.h"

int main(int argc, char* argv[])
{
    ros::init(argc, argv, "laser_analysis_node");
    point_clouds::MyCloud laser;

    ros::Rate loop_rate(40.0);
    ros::Duration(1).sleep();

    while (ros::ok())
    {
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
