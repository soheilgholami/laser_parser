#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <tf/transform_listener.h>
#include <Eigen/Dense>

class LaserTransformer
{
public:
  explicit LaserTransformer(ros::NodeHandle nh)
    : nh_(nh)
  {
    laser_sub_ = nh_.subscribe("/summit_xl_a/front_laser/scan", 1, &LaserTransformer::laserCallback, this);
    laser_pub_ = nh_.advertise<sensor_msgs::LaserScan>("/navigation/laser", 1);
  }

private:
  ros::NodeHandle nh_;
  ros::Subscriber laser_sub_;
  ros::Publisher laser_pub_;
  tf::TransformListener listener_;
  tf::StampedTransform transform_;

  void laserCallback(const sensor_msgs::LaserScanConstPtr& laser_msg)
  {
    listener_.waitForTransform("/summit_xl_a_base_footprint", "/summit_xl_a_front_laser_link",
        ros::Time::now(), ros::Duration(0.1));
        tf::StampedTransform transform;
    try
    {
        listener_.lookupTransform("/summit_xl_a_base_footprint", "/summit_xl_a_front_laser_link",
            ros::Time(0), transform_);

        std::cout << "Translation: "
            << transform_.getOrigin().x() << ", "
            << transform_.getOrigin().y() << ", "
            << transform_.getOrigin().z()
            << std::endl;
        std::cout << "Rotation: x, y, z, w --- "
            << transform_.getRotation().x() << ", "
            << transform_.getRotation().y() << ", "
            << transform_.getRotation().z() << ", "
            << transform_.getRotation().w()
            << std::endl;

        Eigen::Quaterniond q;
        q.x()= transform_.getRotation().x();
        q.y()= transform_.getRotation().y();
        q.z()= transform_.getRotation().z();
        q.w()= transform_.getRotation().w();
        Eigen::Matrix3d R = q.normalized().toRotationMatrix();
        std::cout << "R=" << std::endl << R << std::endl;
    }
    catch (tf::TransformException ex)
    {
        ROS_ERROR("%s",ex.what());
        ros::Duration(1.0).sleep();
    }
  }
};  // End of class LaserTransformer

int main(int argc, char **argv)
{
  ros::init(argc, argv, "laser_tf");
  ros::NodeHandle nh;

  LaserTransformer tranform_laser(nh);

  // Spin until ROS is shutdown
  while (ros::ok())
    ros::spin();

  return 0;
}
