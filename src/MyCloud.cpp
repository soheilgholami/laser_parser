#include "cloud_analysis/MyCloud.h"


namespace point_clouds
{
MyCloud::MyCloud(void) :
    laser_sub_(nh_, "/summit_xl_a/front_laser/scan", 5),
    // laser_sub_(nh_, "/scan_filtered", 5),
    laser_notifier_(laser_sub_, listener_, "summit_xl_a_base_footprint", 5)
{
    counter_laser_ = 0;
    msg_laser_fixed_.data.clear();
    msg_laser_oprime_.data.clear();

    laser_notifier_.setTolerance(ros::Duration(0.01));
    scan_pub_ = nh_.advertise<sensor_msgs::PointCloud>("/my_cloud", 50);

    sb_laser_ = nh_.subscribe("/summit_xl_a/front_laser/scan", 50,
        &MyCloud::cbk_laser,
        this, ros::TransportHints().reliable().tcpNoDelay());

    sb_odom_  =  nh_.subscribe("/summit_xl_a/robotnik_base_control/odom", 50,
        &MyCloud::cbk_odometry, this, ros::TransportHints().reliable().tcpNoDelay());

    pb_laser_fixed_ = nh_.advertise<std_msgs::Float64MultiArray>("/laser_converted/fixed", 1000);
    pb_laser_oprime_ = nh_.advertise<std_msgs::Float64MultiArray>("/laser_converted/oprime", 1000);
}

MyCloud::~MyCloud(void)
{

}

void MyCloud::cbk_odometry(const nav_msgs::Odometry& msg)
{
    Eigen::Vector3d euler = Eigen::Vector3d::Zero();
    geometry_msgs::Quaternion quaternion;
    quaternion.x = msg.pose.pose.orientation.x;
    quaternion.y = msg.pose.pose.orientation.y;
    quaternion.z = msg.pose.pose.orientation.z;
    quaternion.w = msg.pose.pose.orientation.w;
    math_.quaternion2euler(euler, quaternion);
    pose_ << msg.pose.pose.position.x, msg.pose.pose.position.y, euler(2);
}

void MyCloud::kMeansClustering(std::vector<Point>* points, int epochs, int k)
{
    int n = points->size();
    // Randomly initialise centroids
    // The index of the centroid within the centroids vector
    // represents the cluster label.
    std::vector<Point> centroids;
    srand(time(0));
    for (int i = 0; i < k; ++i)
        centroids.push_back(points->at(rand() % n));

    for (int i = 0; i < epochs; ++i)
    {
        // For each centroid, compute distance from centroid to each point
        // and update point's cluster if necessary
        for (std::vector<Point>::iterator c = begin(centroids); c != end(centroids); ++c)
        {
            int clusterId = c - begin(centroids);
            for (std::vector<Point>::iterator it = points->begin(); it != points->end(); ++it)
            {
                Point p = *it;
                double dist = c->distance(p);
                if (dist < p.minDist)
                {
                    p.minDist = dist;
                    p.cluster = clusterId;
                }
                *it = p;
            }
        }
        // Create std::vectors to keep track of data needed to compute means
        std::vector<int> nPoints;
        std::vector<double> sumX, sumY;
        for (int j = 0; j < k; ++j)
        {
            nPoints.push_back(0);
            sumX.push_back(0.0);
            sumY.push_back(0.0);
        }
        // Iterate over points to append data to centroids
        for (std::vector<Point>::iterator it = points->begin(); it != points->end(); ++it)
        {
            int clusterId = it->cluster;
            nPoints[clusterId] += 1;
            sumX[clusterId] += it->x;
            sumY[clusterId] += it->y;
            it->minDist = __DBL_MAX__;  // reset distance
        }
        // Compute the new centroids
        for (std::vector<Point>::iterator c = begin(centroids); c != end(centroids); ++c)
        {
            int clusterId = c - begin(centroids);
            c->x = sumX[clusterId] / nPoints[clusterId];
            c->y = sumY[clusterId] / nPoints[clusterId];
        }
    }
    //
    msg_laser_fixed_.data.clear();
    int cluster_id_now = -1;
    int cluster_id_previous = -1;
    for (std::vector<Point>::iterator it = points->begin(); it != points->end(); ++it)
    {
        cluster_id_now = it->cluster;
        if (cluster_id_now  != cluster_id_previous)
        {
            msg_laser_fixed_.data.push_back(-(cluster_id_now+1)*DENUM);
            msg_laser_oprime_.data.push_back(-(cluster_id_now+1)*DENUM);
        }

        // body to fixed frame =
        Eigen::Matrix4d O_T_B = Eigen::Matrix4d::Identity();
        Eigen::Vector4d tmp1 = Eigen::Vector4d::Zero();
        O_T_B.block<3,3>(0,0) << math_.rot_axis(2, pose_(2));
        O_T_B.block<3,1>(0,3) << pose_(0), pose_(1), 0.0;
        tmp1 << it->x, it->y, 0.0, 1.0;
        tmp1 << O_T_B * tmp1;
        msg_laser_fixed_.data.push_back(tmp1(0));
        msg_laser_fixed_.data.push_back(tmp1(1));

        // body to moving robot frame O'
        Eigen::Matrix4d OP_T_B = Eigen::Matrix4d::Identity();
        Eigen::Vector4d tmp2 = Eigen::Vector4d::Zero();
        OP_T_B.block<3,3>(0,0) << math_.rot_axis(2, -pose_(2));
        OP_T_B.block<3,1>(0,3) << 0, 0, 0;;
        tmp2 << it->x, it->y, 0.0, 1.0;
        tmp2 << OP_T_B * tmp2;
        msg_laser_oprime_.data.push_back(tmp2(0));
        msg_laser_oprime_.data.push_back(tmp2(1));

        cluster_id_previous = cluster_id_now;
    }
     // std::cout << "*****" << std::endl;
    msg_laser_fixed_.data.push_back(5*DENUM);
    pb_laser_fixed_.publish(msg_laser_fixed_);

    msg_laser_oprime_.data.push_back(5*DENUM);
    // pb_laser_oprime_.publish(msg_laser_oprime_);
}

void MyCloud::cbk_laser(const sensor_msgs::LaserScan::ConstPtr& scan)
{
    if (counter_laser_ > 3)
    {
        msg_laser_fixed_.data.clear();
        msg_laser_oprime_.data.clear();

        double angle_min = scan->angle_min;
        double angle_max = scan->angle_max;
        double range_min = scan->range_min;
        double range_max = scan->range_max;
        double angle_increment = scan->angle_increment;
        double steps = (angle_max - angle_min) / angle_increment;
        double angle = angle_min;

        int k = 0; // cluster counter
        double range_current = 0.0;
        double range_previous = 0.0;
        bool validity_flag = false;
        bool validity_flag_previous = false;

        for (int16_t i = 0; i < steps; i++)
        {
            angle += angle_increment;
            range_current = scan->ranges[i];

            validity_flag = (range_current >= range_min) && (range_current <= range_max);
            if (!validity_flag_previous && !validity_flag)
            { // infs
            }
            else if (!validity_flag_previous && validity_flag)
            { // new cluster
                k++;
                msg_laser_fixed_.data.push_back(k*DENUM);
                msg_laser_oprime_.data.push_back(k*DENUM);
            }
            else if (validity_flag_previous && !validity_flag)
            { // cluster finishes
            }
            else if (validity_flag_previous && validity_flag)
            { // non-infs
                if(fabs(range_previous - range_current) > 2)
                {
                    k++;
                    msg_laser_fixed_.data.push_back(k*DENUM);
                    msg_laser_oprime_.data.push_back(k*DENUM);
                }
                else
                {
                    Eigen::Vector4d laser_data = Eigen::Vector4d::Zero();
                    laser_data << range_current * cos(angle), range_current * sin(angle), 0.0, 1.0;

                    // laser to body
                    Eigen::Matrix4d B_T_L = Eigen::Matrix4d::Identity();
                    Eigen::Matrix3d B_R_L = Eigen::Matrix3d::Identity();
                    Eigen::Vector3d B_t_L = Eigen::Vector3d::Zero();
                    B_R_L << 0.707107, 0.707107, 0.0,
                            -0.707107, 0.707107, 0.0,
                             0.0,      0.0,      1.0;
                    B_t_L << 0.377, -0.20957, 0.3274;
                    B_T_L.block<3,3>(0,0) = B_R_L;
                    B_T_L.block<3,1>(0,3) = B_t_L;
                    Eigen::Vector4d B_p = Eigen::Vector4d::Zero();
                    B_p << B_T_L * laser_data;

                    // body to fixed frame
                    Eigen::Matrix4d O_T_B = Eigen::Matrix4d::Identity();
                    Eigen::Vector4d O_p = Eigen::Vector4d::Zero();
                    O_T_B.block<3,3>(0,0) << math_.rot_axis(2, pose_(2));
                    O_T_B.block<3,1>(0,3) << pose_(0), pose_(1), 0.0;
                    O_p << O_T_B * B_p;
                    msg_laser_fixed_.data.push_back(O_p(0));
                    msg_laser_fixed_.data.push_back(O_p(1));

                    // body to moving robot frame O'
                    Eigen::Matrix4d OP_T_B = Eigen::Matrix4d::Identity();
                    Eigen::Vector4d OP_p = Eigen::Vector4d::Zero();
                    OP_T_B.block<3,3>(0,0) << math_.rot_axis(2, -pose_(2));
                    OP_T_B.block<3,1>(0,3) << 0, 0, 0;;
                    OP_p << OP_T_B * B_p;
                    msg_laser_oprime_.data.push_back(OP_p(0));
                    msg_laser_oprime_.data.push_back(OP_p(1));
                }
                //ToDo
            }
            range_previous = range_current;
            validity_flag_previous = validity_flag;
        }
        msg_laser_fixed_.data.push_back((k+1)*DENUM);
        msg_laser_oprime_.data.push_back((k+1)*DENUM);
        pb_laser_fixed_.publish(msg_laser_fixed_);
        pb_laser_oprime_.publish(msg_laser_oprime_);

    }
    counter_laser_++;
}



}// namespace end
