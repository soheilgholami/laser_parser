#ifndef _MY_CLOUD_H_
#define _MY_CLOUD_H_
//
#include <ros/ros.h>
#include <Eigen/Dense>
#include <sensor_msgs/LaserScan.h>
#include <std_msgs/Float64MultiArray.h>
#include "tf/transform_listener.h"
#include "sensor_msgs/PointCloud.h"
#include "tf/message_filter.h"
#include "message_filters/subscriber.h"
#include "laser_geometry/laser_geometry.h"
#include "math/MyMathematics.h"
#include "nav_msgs/Odometry.h"
#include "filters/filter_chain.h"

namespace point_clouds
{
const double DENUM = 10000.0;

struct Point
{
    double x, y;     // coordinates
    int cluster;     // no default cluster
    double minDist;  // default infinite distance to nearest cluster
    Point() : x(0.0), y(0.0), cluster(-1), minDist(__DBL_MAX__) {}
    Point(double x, double y) : x(x), y(y), cluster(-1), minDist(__DBL_MAX__) {}
    double distance(Point p)
    {
        return (p.x - x) * (p.x - x) + (p.y - y) * (p.y - y);
    }
};

class MyCloud
{
public:
    MyCloud(void);
    virtual ~MyCloud(void);

protected:
    bool init(void);
    void kMeansClustering(std::vector<Point>* points, int epochs, int k);

private:
    ros::NodeHandle nh_;
    ros::Subscriber sb_odom_;
    ros::Subscriber sb_laser_;

    ros::Publisher pb_laser_fixed_;
    ros::Publisher pb_laser_oprime_;

    laser_geometry::LaserProjection projector_;
    tf::TransformListener listener_;
    message_filters::Subscriber<sensor_msgs::LaserScan> laser_sub_;
    tf::MessageFilter<sensor_msgs::LaserScan> laser_notifier_;
    ros::Publisher scan_pub_;

    std_msgs::Float64MultiArray msg_laser_fixed_;
    std_msgs::Float64MultiArray msg_laser_oprime_;
    uint16_t counter_laser_;

    math::MyMathematics math_;
    Eigen::Vector3d pose_ = Eigen::Vector3d::Zero();

    void cbk_laser(const sensor_msgs::LaserScan::ConstPtr&);
    void cbk_odometry(const nav_msgs::Odometry&);


};
}
#endif
